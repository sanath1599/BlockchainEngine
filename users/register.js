const init = require('../common/init');

exports.createUser = function (req, res) {

    const multichain = init.getMultichain();
    var key = req.body.data.actor + "-" + req.body.data.userID
    var user_stream = "userstream"

    var new_address = new Promise(function (resolve, reject) {
        multichain.getNewAddress({stream: user_stream}, (err, tx) => {
            resolve(tx)
        })
    })

    new_address.then(function (value) {
        var address = value;
        //Save the details of actor and respective address in the stream_user (Key:Actor and Data:Address)
        multichain.publish({
            stream: user_stream,
            key: key,
            data: new Buffer(address).toString("hex")
        }, (err, tx) => {
            res.json({transactionId: tx});
        })
    })
}


