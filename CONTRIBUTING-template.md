# Contributing to BlockchainEngine
We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features
- Becoming a maintainer

## We Develop with Gitlab
We use gitlab to host code, feature requests as well as accept pull requests ; Trello boards to track issues and ongoing enhancements.

## We Use [Gitlab Flow]https://docs.gitlab.com/ee/topics/gitlab_flow.html, So All Code Changes Happen Through Pull Requests
Pull requests are the best way to propose changes to the codebase (we use [Gitlab Flow]https://docs.gitlab.com/ee/topics/gitlab_flow.html). We actively welcome your pull requests:

Never made an open source contribution before? Wondering how contributions work in the in our project? Here's a quick rundown!
1.	Find an issue that you are interested in addressing or a feature that you would like to add.
2.	Fork the repository associated with the issue to your local Gitlab organization.
3.	Clone the repository to your local machine using git clone git clone https://gitlab.com/gitlab-org/gitlab.git
4.	Create a new branch for your fix using git checkout -b branch-name-here.
5.	Make the appropriate changes for the issue you are trying to address or the feature that you want to add.
6.	Use git add insert-paths-of-changed-files-here to add the file contents of the changed files to the "snapshot" git uses to manage the state of the project, also known as the index.
7.	Use git commit -m "Insert a short message of the changes made here" to store the contents of the index with a descriptive message.
8.	Push the changes to the remote repository using git push origin branch-name-here.
9.	Submit a pull request to the upstream repository.
10.	Title the pull request with a short description of the changes made and the issue or bug number associated with your change. For example, you can title an issue like so "Added more log outputting to resolve #4352".
11.	In the description of the pull request, explain the changes that you made, any issues you think exist with the pull request you made, and any questions you have for the maintainer. It's OK if your pull request is not perfect (no pull request is), the reviewer will be able to help you fix any problems and improve it!
12.	Wait for the pull request to be reviewed by a maintainer.
13.	Make changes to the pull request if the reviewing maintainer recommends them.
14.	Celebrate your success after your pull request is merged!

## Security vulnerability disclosure
Please report suspected security vulnerabilities in private to
`support@gitlab.com`, also see the
disclosure section on the [GitLab.com website](url).
Please do NOT create publicly viewable issues for suspected security
vulnerabilities.

## Helping others

Please help other GitLab users when you can.
The methods people will use to seek help can be found on the [getting help page](url).

Sign up for the mailing list, answer GitLab questions on StackOverflow or
respond in the IRC channel.

## Any contributions you make will be under the MIT Software License
In short, when you submit code changes, your submissions are understood to be under the same [MIT License](http://choosealicense.com/licenses/mit/) that covers the project. Feel free to contact the maintainers if that's a concern.

## License
By contributing, you agree that your contributions will be licensed under its MIT License.

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and expression, level of experience,
nationality, personal appearance, race, religion, or sexual identity and
orientation.

### Our Standards

Examples of behavior that contributes to creating a positive environment
include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or
advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic
  address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting

### Our Responsibilities

Project maintainers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any contributor for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community. Examples of
representing a project or community include using an official project e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a project may be
further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting the project team at [INSERT EMAIL ADDRESS]. All
complaints will be reviewed and investigated and will result in a response that
is deemed necessary and appropriate to the circumstances. The project team is
obligated to maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the project's leadership.

### Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4,
available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/
